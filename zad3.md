---
author: Tomek Janikowski
title: Prezentacja
subtitle: MAJONEZ
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Majonez - informacje ogólne

Majonez to gęsta [emulsja](https://pl.wikipedia.org/wiki/Emulsja "Emulsja") wytwarzana z oleju, [żółtek jajek](https://pl.wikipedia.org/wiki/Jajko_(kulinaria) "Jajko (kulinaria)") i dodatków smakowych, o szerokim zastosowaniu kulinarnym.

Początkowo wyrabiany domowo (ręcznie) na bieżące potrzeby, od początku XX wieku także przemysłowo, w postaci do dłuższego przechowywania. Głównym składnikiem klasycznego majonezu jest [oliwa](https://pl.wikipedia.org/wiki/Olej_oliwkowy "Olej oliwkowy") z dodatkiem surowego żółtka, jako składnika emulgującego, przyprawiona [octem winnym](https://pl.wikipedia.org/wiki/Ocet_winny "Ocet winny") lub [cytryną](https://pl.wikipedia.org/wiki/Cytryna_zwyczajna "Cytryna zwyczajna"), [solą](https://pl.wikipedia.org/wiki/S%C3%B3l_kuchenna "Sól kuchenna") i białym [pieprzem](https://pl.wikipedia.org/wiki/Pieprz "Pieprz").

<img title="" src="file:///C:/Users/tomasz.janikowski/zad3/1.jpg" alt="majonez" width="453" data-align="center">

---



## Majonez - więcej informacji

Obecnie w praktyce kuchennej oraz w produkcji przemysłowej spotyka się różne warianty majonezu i różne odpowiadające im przepisy (np. majonez z [musztardą diżońską](https://pl.wikipedia.org/wiki/Musztarda_di%C5%BCo%C5%84ska "Musztarda diżońska") – *mayonnaise à la moutarde de Dijon*). Przede wszystkim oliwa zastępowana jest zwykle innymi [olejami roślinnymi](https://pl.wikipedia.org/wiki/Oleje_ro%C5%9Blinne "Oleje roślinne"). Zamiast surowego żółtka stosuje się czasem żółtka gotowane lub całe surowe jajka, a w produkcji przemysłowej – jajka w proszku. Często spotykanym dodatkiem jest [musztarda](https://pl.wikipedia.org/wiki/Musztarda "Musztarda"), która pełni rolę stabilizatora emulsji oraz dodatku smakowego. Musztarda zaostrza jednak smak majonezu, co jest krytykowane przez ortodoksyjnych znawców kuchni. Spotyka się też w majonezie najrozmaitsze inne przyprawy, np. [cukier](https://pl.wikipedia.org/wiki/Cukier_spo%C5%BCywczy "Cukier spożywczy"). Do majonezów produkowanych przemysłowo dodaje się także stabilizatory, konserwanty, barwniki i aromaty syntetyczne.

W [kuchni francuskiej](https://pl.wikipedia.org/wiki/Kuchnia_francuska "Kuchnia francuska") oraz w dawniejszej [kuchni polskiej](https://pl.wikipedia.org/wiki/Kuchnia_polska "Kuchnia polska") majonezem (fr. *mayonnaise*) nazywa się też potrawę: *na zimno z ryby, raków lub mięsa i drobiu, która podaje się albo jako przekąska w formie sałaty na muszelkach, albo na półmiskach w całości jako danie wykwintne przy proszonych śniadaniach, obiadach i kolacjach, najczęściej z ryb. Wybiera się wtedy dużą rybę, gotuje się ją w całości w wanience, a ułożywszy na długim półmisku lub na desce serwetą obszytej, smaruje się ją grubo sosem majonezowym i garniruje po wierzchu w najrozmaitszy sposób* (cyt. za [Marią Ochorowicz-Monatową](https://pl.wikipedia.org/wiki/Maria_Ochorowicz-Monatowa "Maria Ochorowicz-Monatowa")).

Jako najważniejszy sos zimny majonez stanowi podstawę wielu innych sosów, m.in. andaluzyjskiego, [tatarskiego](https://pl.wikipedia.org/wiki/Sos_tatarski "Sos tatarski"), musztardowego, rdzawego, zielonego (ze szpinakiem), sardalaise, [gribiche](https://pl.wikipedia.org/wiki/Gribiche "Gribiche"), ravigote, [remoulade](https://pl.wikipedia.org/wiki/Remoulade "Remoulade") (inaczej sosu duńskiego), a także sosu rosyjskiego na zimno.

---



## Pochodzenie nazwy

Istnieją rozmaite wersje anegdotycznego przekazu łączącego wprowadzenie majonezu do kuchni francuskiej z miastem [Mahón](https://pl.wikipedia.org/wiki/Ma%C3%B3-Mah%C3%B3n "Maó-Mahón") na [Minorce](https://pl.wikipedia.org/wiki/Minorka "Minorka"), z osobą [marszałka Francji](https://pl.wikipedia.org/wiki/Marsza%C5%82kowie_Francji "Marszałkowie Francji"), księcia [Louis François Armand du Plessis de Richelieu](https://pl.wikipedia.org/wiki/Louis_Fran%C3%A7ois_Armand_du_Plessis "Louis François Armand du Plessis") oraz z odniesionym tam zwycięstwem nad Anglikami w 1756 roku. Dla jego upamiętnienia admirał miał tak właśnie nazwać wynaleziony wtedy przez siebie sos.

Według innej tradycji jest on pomysłem angielskich obrońców obleganego przez Francuzów Mahón, którym jako pożywienie pozostały tylko zapasy jaj i oliwy. W tych warunkach ich głównym pokarmem stały się jaja na twardo przyprawiane sosem sporządzonym z mieszaniny jaj i oliwy. Gdy Francuzi zdobyli Mahón, zastali tam pożywienie, w którym również zasmakowali i które sami później upowszechnili.

Odmienny przekaz głosi, że nazwa „majonez” pochodzi od [Karola Gwizjusza](https://pl.wikipedia.org/wiki/Gwizjusze "Gwizjusze") księcia [Mayenne](https://pl.wikipedia.org/wiki/Mayenne_(departament) "Mayenne (departament)"), który miał pożywić się kurczakiem podanym z takim zimnym sosem, przed bitwą stoczoną z [Henrykiem IV](https://pl.wikipedia.org/wiki/Henryk_IV_Wielki "Henryk IV Wielki") pod [Arques](https://pl.wikipedia.org/wiki/Arques-la-Bataille "Arques-la-Bataille") w 1589 roku.

<img src="https://images.immediate.co.uk/production/volatile/sites/30/2020/08/mayonnaise-8a626ad.jpg" title="" alt="majonez2" data-align="center">

Natomiast ludowa etymologia wywodzi nazwę sosu od francuskiego miasta [Bayonne](https://pl.wikipedia.org/wiki/Bajonna "Bajonna") (słynącego ze swej [szynki](https://pl.wikipedia.org/wiki/Szynka "Szynka")) dowodząc, iż jego pierwotna nazwą był *sauce bayonnaise*.

Mistrz kuchni [Marie-Antoine Carême](https://pl.wikipedia.org/wiki/Marie-Antoine_Car%C3%AAme "Marie-Antoine Carême") w swej książce *Cuisinier parisien* („Kucharz paryski”) z 1833 r. wyprowadza obecną nazwę (w zapisie *magnonnaise* lub *magnionnaise*), od francuskiego słowa *manier* – „mieszać”. Natomiast inny słynny kucharz [Prosper Montagné](https://pl.wikipedia.org/wiki/Prosper_Montagn%C3%A9 "Prosper Montagné") wskazuje na starofrancuskie słowo *moyeu* – „żółtko” jako źródłosłów tej nazwy.

---

![majonezy](C:\Users\tomasz.janikowski\zad3\2.webp)

### Który majonez jest najlepszy?

TOP5 najpopularniejszych majonezów w Polsce:

1. Majonez Dekoracyjny Winiary

2. Majonez Kielecki

3. Majonez Hellman's

4. Majonez Pudliszki

5. Majonez Madero (marki własnej Biedronki)



Osobiście uważam, że nie ma jednego najlepszego majonezu, aczkolwiej, moim skromnym zdaniem, na uwagę zasługują lokalni producenci.

---

## Lokalni producenci

- Na Dolnym Śląsku można spotkać zagorzałych fanów **Majonezu Świdnickiego**.
- Na Warmii i Mazurach nie brakuje wielbicieli **Majonezu Kętrzyńskiego**.
- Pomorzanie zajadają się... **Majonezem Pomorskim**.
- Na Podlasiu jest wielu miłośników **Majonezu Napoleońskiego**.
- W Łodzi i okolicach jest sporo osób sięgających po **Majonez Motyl**.

![kętrzyński](C:\Users\tomasz.janikowski\zad3\3.webp)

---



## Przykładowe ceny majonezu

| Marka       | Pojemność | Cena     |
| ----------- |:---------:|:--------:|
| Kętrzyński  | 0,31 l    | 6,49 zł  |
| Dekoracyjny | 0,4 l     | 7,99 zł  |
| Kielecki    | 0,31 l    | 8,99 zł  |
| Napoleoński | 0,9 l     | 18,49 zł |
